const {User,Biodata,History} = require('../../models')
const { join } = require('path')
const bcrypt = require("bcrypt")
const user = require('../../models/user')

class HomeController {

    index = (req,res) => {
        User.findAll()
        .then(users => {
            res.render(join(__dirname, '../../views/index')),{
                content: './pages/userList',
                users: users
            }
        })
    }

    add = (req,res) => {
        res.render(join(__dirname, '../../views/index'),{
            content: './pages/userAdd',
        })
    }

    saveUser = async (req,res) => {
        const salt = await bcrypt.genSalt(10)
        User.create({
            name: req.body.name, 
            username: req.body.username,
            age: req.body.age,
            password: await bcrypt.hash(req.body.password, salt),
            user_biodata: {
                address: req.body.address,
                gender: req.body.gender
            }
        })
        .then(()=>{
            res.redirect('/')
        }).catch(err => {
            console.log(err)
        })
    }

    edit = (req,res) => {
        User.findOne({
            where: {
                id: req.params.id
            },
            include: 'biodata'
        }).then(()=>{
            res.render(join(__dirname, '../../views/index'),{
                content: './pages/userEdit',
                user:user
            })
        })
    }

    saveEdit = (req,res) => {
        Biodata.update({ 
            name: req.body.name,
            age: req.body.age,
            username: req.body.username,
            password: req.body.password
        }).then(user =>{
            res.redirect('/')
        }).catch(err => {
            res.status(422).json("Can't update user")
        })
    }

    score = (req,res) => {
        User.findOne({
            where: {
                id: req.params.id,
            },
            include: 'history'
        }).then(user => {
            res.render(join(__dirname,'../../views/index'),{
                content:'./pages/userSkor',
                user:user
            })
        })
    }

    saveScore = (req,res) => {
        History.create({
            score: req.body.score,
            id: req.params.id,
        })
        .then(user => {
            res.redirect('/')
        }) .catch(err => {
            res.status(422).json("Can't update score")
        })
    }

    delete = (req, res) => {
        User.destroy({
            where: {
                id: req.params.id,
            }
        })
        .then(() => {
            res.redirect('/')
        })
    } 

}
module.exports = HomeController
