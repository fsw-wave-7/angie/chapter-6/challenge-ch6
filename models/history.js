'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class History extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      History.belongsTo(models.User, {
        as: "user_history",
        foreignKey: "id",
        onDelete: "CASCADE",
      });
    }
  };
  History.init({
    name: DataTypes.STRING,
    username: DataTypes.STRING,
    score: DataTypes.INTEGER,
    time: DataTypes.STRING,
    activity: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'History',
  });
  return History;
};